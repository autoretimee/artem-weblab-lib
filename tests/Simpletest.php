<?php
class SimpleTest extends PHPUnit\Framework\TestCase
{
  public function testSimpleCalculate()
  {
    $simple = new \Artem\Simple(50000, 10, 3);
    $result = $simple->calculate();
    $this->assertEquals(65000, $result);
  }
} 