<?php
class CompoundTest extends PHPUnit\Framework\TestCase
{
  public function testCompoundCalculate()
  {
    $compound = new \Artem\Compound(50000, 10, 3);
    $result = $compound->calculate();
    $this->assertEquals(66550, $result);
  }
}
